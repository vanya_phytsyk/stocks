package com.example.stocks.domain

import com.example.stocks.model.Period
import com.example.stocks.model.StockData

interface StockRepository {
     fun getStockData(period: Period): List<StockData>
}