package com.example.stocks.domain

import com.example.stocks.model.Period
import com.example.stocks.model.StockData
import com.example.stocks.model.StockPerformance

class StockPerformanceUseCase(private val repository: StockRepository) {
    suspend operator fun invoke(period: Period): List<StockPerformance> {
        val stockData = repository.getStockData(period)
        return stockData.map { it.calculatePerformance() }
    }

    private fun StockData.calculatePerformance(): StockPerformance {
        //to calculate stock performance I use closure rates
        val initialValue = closures.first()
        val performance = closures.map {
            (it - initialValue) / initialValue * 100
        }
        return StockPerformance(this, performance)
    }

}