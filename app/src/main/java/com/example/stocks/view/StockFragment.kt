package com.example.stocks.view

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.viewModelScope
import com.example.stocks.R
import com.example.stocks.data.StockRepositoryImpl
import com.example.stocks.domain.StockPerformanceUseCase
import com.example.stocks.presentation.StockPerformanceViewModel
import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.utils.EntryXComparator
import com.github.mikephil.charting.utils.ViewPortHandler
import kotlinx.android.synthetic.main.fragment_main.*
import kotlinx.android.synthetic.main.fragment_main.view.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.time.format.DateTimeFormatter
import java.util.*


@FlowPreview
@ExperimentalCoroutinesApi
class StockFragment : Fragment() {

    private lateinit var stockPerformanceViewModel: StockPerformanceViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        stockPerformanceViewModel =
            ViewModelProviders.of(this).get(StockPerformanceViewModel::class.java).apply {
                getPerformance = StockPerformanceUseCase(StockRepositoryImpl(requireContext()))
                setIndex(arguments?.getInt(ARG_SECTION_NUMBER) ?: 1)
            }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_main, container, false)

        root.chart.xAxis.valueFormatter = object : ValueFormatter() {

            override fun getFormattedValue(value: Float): String {
                return DATE_FORMATTER.format(Date().also { it.time = value.toLong() })
            }

        }

        root.chart.xAxis.granularity = (24 * 60 * 60 * 1000).toFloat()
        root.chart.xAxis.isGranularityEnabled = true


        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        stockPerformanceViewModel.viewModelScope.launch {
            stockPerformanceViewModel.state.filter { it.second.isNotEmpty() }.collect {
                val stockData = it.second[it.first]
                val stockPerformance = stockData.performance
                val chartEntries =
                    stockData.stockData.closures.mapIndexed { index, closure ->
                        Entry(
                            stockData.stockData.timestamps[index].toFloat(),
                            closure.toFloat(),
                            stockPerformance[index]
                        )
                    }.sortedWith(EntryXComparator())
                val dataSet = LineDataSet(chartEntries, "Stock rate")
                dataSet.valueFormatter = object: ValueFormatter() {
                    override fun getFormattedValue(value: Float): String {
                        return "$value (2.34%)"
                    }
                }
                dataSet.valueTextSize = 14.0F
                dataSet.lineWidth = 5.0F
                dataSet.mode = LineDataSet.Mode.LINEAR
                dataSet.color = Color.GREEN
                val lineData = LineData(dataSet)
                chart.data = lineData
                chart.invalidate()
            }
        }

    }

    override fun onStart() {
        super.onStart()
        stockPerformanceViewModel.fetchStockPerformance()
    }

    companion object {

        private const val ARG_SECTION_NUMBER = "section_number"
        private val DATE_FORMATTER = SimpleDateFormat("dd MMM", Locale.US)


        @JvmStatic
        fun newInstance(sectionNumber: Int): StockFragment {
            return StockFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_SECTION_NUMBER, sectionNumber)
                }
            }
        }
    }
}