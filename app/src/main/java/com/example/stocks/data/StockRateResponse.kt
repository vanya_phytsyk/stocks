package com.example.stocks.data

import kotlinx.serialization.Serializable

@Serializable
data class StockRateResponse(
    val content: Content,
    val status: String
)