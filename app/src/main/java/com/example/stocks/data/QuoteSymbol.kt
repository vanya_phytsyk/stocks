package com.example.stocks.data

import kotlinx.serialization.Serializable

@Serializable
data class QuoteSymbol(
    val closures: List<Double>,
    val highs: List<Double>,
    val lows: List<Double>,
    val opens: List<Double>,
    val symbol: String,
    val timestamps: List<Long>,
    val volumes: List<Long>
)