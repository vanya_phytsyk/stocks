package com.example.stocks.data

import kotlinx.serialization.Serializable

@Serializable
data class Content(
    val quoteSymbols: List<QuoteSymbol>
)