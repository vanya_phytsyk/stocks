package com.example.stocks.data

import android.content.Context
import com.example.stocks.domain.StockRepository
import com.example.stocks.model.Period
import com.example.stocks.model.StockData
import kotlinx.serialization.builtins.list
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration

class StockRepositoryImpl(val context: Context) : StockRepository {
    override fun getStockData(period: Period): List<StockData> {
        val fileToRead =
            when (period) {
                Period.MONTHLY -> "responseQuotesMonth.json"
                else -> "responseQuotesWeek.json"
            }
        val fileText = context.assets.open(fileToRead).bufferedReader().use { it.readText() }
        val json = Json(JsonConfiguration.Default)
        val response = json.parse(StockRateResponse.serializer(), fileText)

        return response.content.quoteSymbols.map {
            it.convert()
        }
    }
}

private fun QuoteSymbol.convert(): StockData {
    return StockData(this.symbol, closures, highs, lows, opens, timestamps.map { it * 1000 }, volumes)
}
