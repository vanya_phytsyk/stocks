package com.example.stocks.model

import com.example.stocks.model.StockData

class StockPerformance(val stockData: StockData, val performance: List<Double>) {
}
