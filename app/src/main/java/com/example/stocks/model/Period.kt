package com.example.stocks.model

enum class Period {
    MONTHLY, WEEKLY
}