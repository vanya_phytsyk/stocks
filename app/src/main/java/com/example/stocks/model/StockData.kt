package com.example.stocks.model

data class StockData(
    val stockSymbol: String,
    val closures: List<Double>,
    val highs: List<Double>,
    val lows: List<Double>,
    val opens: List<Double>,
    val timestamps: List<Long>,
    val volumes: List<Long>
)
