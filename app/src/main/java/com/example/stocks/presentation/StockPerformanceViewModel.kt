package com.example.stocks.presentation

import android.util.Log
import androidx.lifecycle.*
import com.example.stocks.domain.StockPerformanceUseCase
import com.example.stocks.model.Period
import com.example.stocks.model.StockPerformance
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.launch

@ExperimentalCoroutinesApi
@FlowPreview
class StockPerformanceViewModel : ViewModel() {

    lateinit var getPerformance: StockPerformanceUseCase

    private val index = ConflatedBroadcastChannel<Int>(0)

    private val stockData = ConflatedBroadcastChannel<List<StockPerformance>>(emptyList())

    val state = combine(index.asFlow(), stockData.asFlow()) { index, stockData ->
        return@combine index to stockData
    }

    fun setIndex(index: Int) {
        this.index.offer(index)
    }

    fun fetchStockPerformance() {
        viewModelScope.launch(Dispatchers.Default) {
            val performance = getPerformance(Period.WEEKLY)
            stockData.offer(performance)
        }
    }

}